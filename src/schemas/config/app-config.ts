import Joi from 'joi'
import {ServerConfigSchema} from './server'

export const AppConfigSchema = Joi.object({
  server: ServerConfigSchema.required(),
  services: Joi.object({
    // list the services and the things they need to run
  }).required()
}).meta({className: 'AppConfig'})
