import Joi from 'joi'

// TODO: This could be moved into the opinionated-hapi-core, to be imported and
// possibly extended. IE  
// import {ServerConfigSchema:ServerConfigDefaultSchema} from '...'
// export const ServerConfigSchema= ServerConfigDefaultSchema.keys({extra:Jo})API_SECRET

// TODO: joi-to-typescript and hapi-swagger know how to walk an object looking
// for .example() examples.  Work out how to to this and use these examples for
// tests...

const ServerConfigSettingSchema = Joi.object({
  PORT: Joi.number().description('port server listens on').example(4000)
})

const ServerConfigSecretSchema = Joi.object({
  API_SECRET: Joi.string()
    .description('Secret passed in header to prove request came via api gateway')
    .example('hArdt0Gues5')
})

export const ServerConfigSchema = ServerConfigSettingSchema.concat(ServerConfigSecretSchema).meta({
  className: 'ServerConfig'
})
