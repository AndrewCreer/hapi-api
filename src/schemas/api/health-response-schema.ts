import Joi from 'joi'

const label = 'HeatlhResponse'
export const HealthResponseSchema = Joi.object({
  ok: Joi.boolean().example(true).description('The status of the server')
})
  .label(label) // hapi-swagger
  .meta({className: label}) // joi-to-typescript
