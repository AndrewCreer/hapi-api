import Joi from "joi";

// The database will do the id
export const GooseCreateSchema = Joi.object({
  name: Joi.string().required(),
  colour: Joi.string().required(),
}).meta({ className: "GooseCreate" });

// This is how we extend Joi
export const GooseSchema = GooseCreateSchema.keys({
  id: Joi.string().guid().required(),
}).meta({ className: "Goose" });
