import {init, start} from './server/server'

init().then(server => start(server))
