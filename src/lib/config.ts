/**
 * In which we
 *  - read in the process.env ONCE (as the app starts)
 *  - Look through the AppConfigSchema for CAPITALIZED (?) things for the things we need
 *  - create a readonly Map of the values
 *  -
 **/

import {AppConfigSchema} from '../schemas/config/app-config'

import type {AppConfig} from '../interfaces/config'

export class Config {
  private static config: AppConfig

  public static get(): AppConfig {
    if (this.config) return this.config

    this.config = {
      server: {
        PORT: parseInt(process.env['PORT'] as string)
      },
      services: {}
    }

    const valid = AppConfigSchema.validate(this.config)
    if (valid.error) {
      // Can't use a logger here. The logger needs the config to know how to work
      // Dont log the whole valid, or we leak the correct stuff
      console.error(JSON.stringify(valid.error.details, null, ' '))
      throw new Error('Config was not validated. See details on stdout')
    }
    return this.config
  }
}
