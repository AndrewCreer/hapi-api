import type {HeatlhResponse} from '../../interfaces/api'
import type {RequestContext} from '../logger'
import {logger} from '../logger'

/**
 * Services implement the business logic
 * the ctx allows logging to happen, should not be used to for anything else.
 *
 * IE explicitly pass the stuff you need
 */
export class HealthService {
  // Every thing you need to log and handle the request
  public static async handle(ctx: RequestContext): Promise<HeatlhResponse> {
    logger.debug('Starting Heath Response', ctx)
    return {ok: true}
  }
}
