import type {Request} from '@hapi/hapi'
import {createLogger, format, transports} from 'winston'
import {omit} from 'lodash'

// We log to stdout all the same way, regardless of being in a hapi bit or a
// service.
const {combine, timestamp, json} = format

const pack = require('../../package.json')
const {name, version} = pack

export const logger = createLogger({
  format: combine(timestamp(), json()),
  defaultMeta: {package: {name, version}},
  transports: [new transports.Console()]
})

export const requestContext = ({path, info, method, headers}: Request) => ({
  path,
  method,
  requestId: info.id,
  headers: omit(headers, ['user-agent', 'accept'])
})

export type RequestContext = ReturnType<typeof requestContext>
