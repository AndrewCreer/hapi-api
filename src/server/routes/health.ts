import type {ServerRoute, Request} from '@hapi/hapi'
import {HealthResponseSchema} from '../../schemas/api/health-response-schema'
import {logger, requestContext} from '../../lib/logger'
import {HealthService} from '../../lib/services/health'

export const HealthRoute: ServerRoute = {
  method: 'GET',
  path: '/health',
  options: {
    tags: ['api'],
    description: 'health endpoint',
    response: {schema: HealthResponseSchema, sample: 1}
  },
  handler: async (r: Request) => {
    const ctx = requestContext(r)
    logger.debug({message: 'starting hapi health', ...ctx})
    return await HealthService.handle(ctx)
  }
}
