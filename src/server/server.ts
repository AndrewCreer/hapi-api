import type {Server} from '@hapi/hapi'
import * as Hapi from '@hapi/hapi'
import {logger} from '../lib/logger'
import {HealthRoute} from './routes/health'
import {Config} from '../lib/config'

export const init = async () => {
  const c = Config.get()
  const server = Hapi.server({
    port: c.server.PORT,
    host: '0.0.0.0'
  })

  return server
}

export const start = async (server: Server) => {
  server.route(HealthRoute)

  logger.info(`Server Listening on ${server.info.uri}`)

  return server.start()
}
