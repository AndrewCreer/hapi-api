import type {Server} from '@hapi/hapi'
import {init, start} from '../../src/server/server'
import request from 'supertest'

describe('Smoke test', () => {
  let server: Server
  beforeEach(async () => {
    server = await init()
    await start(server)
  })
  afterEach(async () => {
    await server.stop()
  })
  it(`should have a '/health' route`, done => {
    request(server.listener)
      .get('/health')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body).toMatchObject({ok: true})
        done()
      })
  })
})
