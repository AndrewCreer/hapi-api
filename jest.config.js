/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

module.exports = {
  ...require('@mrgoose/opinionated-hapi-builder/jest.config.js')
}
