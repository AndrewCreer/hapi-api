# Hapi-api

Demo of DRY api.

 - Hapi uses Joi schemas for validation
 - Joi schemas are composeable
 - joi-to-typescript converts schemas to interfaces exactly. 
   - Type Saftey 
   - validation 
   - runtime checking
 - hapi-swagger generates swagger using Joi Schemas


 ```
# Dev
yarn 
yarn dev </dev/null &
curl localhost:4000/health

# Run built version
yarn build
du -hd0 node_modules
 176M	node_modules
yarn install --production
du -hd0 node_modules # much smaller
 10M	node_modules
yarn start &
curl localhost:4000/health

 ```
